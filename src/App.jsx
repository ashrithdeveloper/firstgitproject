import React ,{useState} from 'react'

export default function App() {

  const [rows , setRows] = useState([])

  const addRow = () => {
    setRows([...rows, { name: "", price: "" }])
  };

  const handleChange = (index, e) => {
    const { name, value } = e.target
    const updatedRows = [...rows]
    updatedRows[index][name] = value
    setRows(updatedRows)
    console.log(rows)
  };


  return (
    <>
    <button onClick={addRow}>add</button>
     <table border="2" width="500px">
      <thead>
        <tr>
          <th>Name</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
      {rows.map((val, index) => {
        return (
          <tr key={index}>
          <td>
            <input 
              type="text" 
              name="name" 
              onChange={(e) => { handleChange(index, e)}} 
            />
          </td>
          <td>
            <input 
              type="number" 
              name="name" 
              onChange={(e) => { handleChange(index, e)}} 
            />
          </td>
        </tr>
        )
      })}
        
      </tbody>
     </table>
    </>
  )
}
